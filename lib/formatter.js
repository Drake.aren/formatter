'use babel';

import FormatterView from './formatter-view';
import { CompositeDisposable } from 'atom';
import path from 'path'

export default {

  formatterView: null,
  subscriptions: null,

  activate(state) {
    this.formatterView = new FormatterView(state.formatterViewState);
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.formatterView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'formatter:format': () => this.format()
    }));
  },

  deactivate() {
    this.modalPanel.destroy();
    this.subscriptions.dispose();
    this.formatterView.destroy();
  },

  serialize() {
    return {
      formatterViewState: this.formatterView.serialize()
    };
  },

  format () {
    let editor = atom.workspace.getActiveTextEditor()
    let extension = path.extname(editor.getTitle())
    extension = extension.slice(1, extension.length)
    let acutalFormatter = require('../formatters/' + extension + '.js')
    let newText = acutalFormatter.onFormat(editor.getText(), editor.getPath())
    if (newText != null) {
      editor.setText(newText)
    }
  }

};
