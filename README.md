# Formatter

# Create your own
Simple create file in `./formatters` folder with name as extensions of your language

example
```shell
#for javascript file's
touch formatters/js.js
#for clojure file's
touch formatters/clojure.js
```

in it paste simple code that will be callback on your language file format
```javascript
module.exports = {
  onFormat: (code, path) => {
    return code
  }
}
```
return format code or return `null` for change file by external command, like `eslint`
